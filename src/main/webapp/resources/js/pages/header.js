function LocationController($scope, $location) {
    if($location.$$absUrl.lastIndexOf('/contacts') > 0){
        $scope.activeURL = 'contacts';
    } else if($location.$$absUrl.lastIndexOf('/calcados') > 0){
    	$scope.activeURL = 'calcados';
    } else {
        $scope.activeURL = 'home';
    }
}