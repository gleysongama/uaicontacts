<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="addCalcadosModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="addCalcadosModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addCalcadosModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="calcado" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newCalcadoForm" novalidate>
			<div class="pull-left">
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.estilo" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required autofocus ng-model="calcado.estilo"
							name="estilo"
							placeholder="<spring:message code='calcado'/>&nbsp;<spring:message code='calcados.estilo'/>" />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && newCalcadoForm.estilo.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.cor" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.cor" name="cor"
							placeholder="<spring:message code='calcados.cor'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && newCalcadoForm.cor.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.marca" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.marca" name="marca"
							placeholder="<spring:message code='calcados.marca'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && newCalcadoForm.marca.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.loja" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.loja" name="loja"
							placeholder="<spring:message code='calcados.loja'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && newCalcadoForm.loja.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.valor" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.valor" name="valor"
							placeholder="<spring:message code='calcados.valor'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && newCalcadoForm.valor.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<input type="submit" class="btn btn-inverse"
					ng-click="createCalcado(newCalcadoForm);"
					value='<spring:message code="create"/>' />
				<button class="btn btn-inverse" data-dismiss="modal"
					ng-click="exit('#addCalcadosModal');" aria-hidden="true">
					<spring:message code="cancel" />
				</button>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="updateCalcadosModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="updateCalcadosModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="updateCalcadosModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="calcado" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="updateCalcadoForm" novalidate>
			<input type="hidden" required ng-model="calcado.id" name="id"
				value="{{calcado.id}}" />

			<div class="pull-left">
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.estilo" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" autofocus required ng-model="calcado.estilo"
							name="estilo"
							placeholder="<spring:message code='calcado'/>&nbsp;<spring:message code='calcados.estilo'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateCalcadoForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.cor" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.cor" name="cor"
							placeholder="<spring:message code='calcados.cor'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateCalcadoForm.cor.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.marca" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.marca"
							name="marca"
							placeholder="<spring:message code='calcado.marca'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateCalcadoForm.marca.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.loja" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.loja"
							name="marca"
							placeholder="<spring:message code='calcado.loja'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateCalcadoForm.loja.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<div>
					<div class="input-append">
						<label>* <spring:message code="calcados.valor" />:
						</label>
					</div>
					<div class="input-append">
						<input type="text" required ng-model="calcado.valor"
							name="marca"
							placeholder="<spring:message code='calcado.valor'/> " />
					</div>
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateCalcadoForm.valor.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<input type="submit" class="btn btn-inverse"
					ng-click="updateCalcado(updateCalcadoForm);"
					value='<spring:message code="update"/>' />
				<button class="btn btn-inverse" data-dismiss="modal"
					ng-click="exit('#updateCalcadosModal');" aria-hidden="true">
					<spring:message code="cancel" />
				</button>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="deleteCalcadosModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchCalcadosModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteCalcadosModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="calcado" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteCalcadoForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{calcado.estilo}} / {{calcado.cor}} / {{calcado.marca}}?
			</p>

			<input type="submit" class="btn btn-inverse"
				ng-click="deleteCalcado();" value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteCalcadosModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>

<div id="searchCalcadosModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchCalcadosModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchCalcadosModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="searchCalcadoForm" novalidate>
			<label><spring:message code="search.for" /></label>

			<div>
				<div class="input-append">
					<input type="text" autofocus required ng-model="searchFor"
						name="searchFor"
						placeholder="<spring:message code='calcado'/>&nbsp;<spring:message code='calcados.cor'/> " />
				</div>
				<div class="input-append displayInLine">
					<label class="displayInLine"> <span
						class="alert alert-error"
						ng-show="displayValidationError && searchCalcadoForm.searchFor.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<input type="submit" class="btn btn-inverse"
				ng-click="searchCalcado(searchCalcadoForm, false);"
				value='<spring:message code="search"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#searchCalcadosModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
