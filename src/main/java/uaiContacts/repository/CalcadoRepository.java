package uaiContacts.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import uaiContacts.model.Calcado;

public interface CalcadoRepository extends PagingAndSortingRepository<Calcado, Integer> {
	Page<Calcado> findByCorLike(Pageable pageable, String cor);
	/*Page<Calcado> findByEstiloLike(Pageable pageable, String estilo);
	Page<Calcado> findByMarcaLike(Pageable pageable, String marca);
	Page<Calcado> findByLojaCompraLike(Pageable pageable, String lojaCompra);
	Page<Calcado> findByValorCompraLike(Pageable pageable, String valorCompra);
	Page<Calcado> findByDataCompraLike(Pageable pageable, String dataCompra);*/
}
