package uaiContacts.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Calcado {
	
	@Id
    @GeneratedValue
    private int id;
	private String estilo;
	private String cor;
	private String marca;
	private String loja;
	private String valor;
	
	public Calcado() {
		
	}

	public Calcado(String estilo, String cor, String marca, String loja,
			String valor, int id) {
		super();
		this.estilo = estilo;
		this.cor = cor;
		this.marca = marca;
		this.loja = loja;
		this.valor = valor;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getLoja() {
		return loja;
	}

	public void setLoja(String loja) {
		this.loja = loja;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
    public boolean equals(Object object) {
        if (object instanceof Calcado){
            Calcado calcado = (Calcado) object;
            return calcado.id == id;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return id;
    }

}
