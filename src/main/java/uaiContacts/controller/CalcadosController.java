package uaiContacts.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import uaiContacts.model.Calcado;
import uaiContacts.service.CalcadoService;
import uaiContacts.vo.CalcadoListVO;

import java.util.Locale;

@Controller
@RequestMapping(value = "/protected/calcados")
public class CalcadosController {

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

    @Autowired
    private CalcadoService calcadoService;

    @Autowired
    private MessageSource messageSource;

    @Value("5")
    private int maxResults;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("calcadosList");
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listAll(@RequestParam int page, Locale locale) {
        return createListAllResponse(page, locale);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(@ModelAttribute("calcado") Calcado calcado,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        calcadoService.save(calcado);

        if (isSearchActivated(searchFor)) {
            return search(searchFor, page, locale, "message.create.success");
        }

        return createListAllResponse(page, locale, "message.create.success");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("id") int calcadoId,
                                    @RequestBody Calcado calcado,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        if (calcadoId != calcado.getId()) {
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }

        calcadoService.save(calcado);

        if (isSearchActivated(searchFor)) {
            return search(searchFor, page, locale, "message.update.success");
        }

        return createListAllResponse(page, locale, "message.update.success");
    }

    @RequestMapping(value = "/{calcadoId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> delete(@PathVariable("calcadoId") int calcadoId,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {


        try {
            calcadoService.delete(calcadoId);
        } catch (AccessDeniedException e) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }

        if (isSearchActivated(searchFor)) {
            return search(searchFor, page, locale, "message.delete.success");
        }

        return createListAllResponse(page, locale, "message.delete.success");
    }

    @RequestMapping(value = "/{cor}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@PathVariable("cor") String cor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        return search(cor, page, locale, null);
    }

    private ResponseEntity<?> search(String cor, int page, Locale locale, String actionMessageKey) {
        CalcadoListVO calcadoListVO = calcadoService.findByCorLike(page, maxResults, cor);

        if (!StringUtils.isEmpty(actionMessageKey)) {
            addActionMessageToVO(calcadoListVO, locale, actionMessageKey, null);
        }

        Object[] args = {cor};

        addSearchMessageToVO(calcadoListVO, locale, "message.search.for.active", args);

        return new ResponseEntity<CalcadoListVO>(calcadoListVO, HttpStatus.OK);
    }

    private CalcadoListVO listAll(int page) {
        return calcadoService.findAll(page, maxResults);
    }

    private ResponseEntity<CalcadoListVO> returnListToCalcado(CalcadoListVO calcadoList) {
        return new ResponseEntity<CalcadoListVO>(calcadoList, HttpStatus.OK);
    }

    private ResponseEntity<?> createListAllResponse(int page, Locale locale) {
        return createListAllResponse(page, locale, null);
    }

    private ResponseEntity<?> createListAllResponse(int page, Locale locale, String messageKey) {
        CalcadoListVO calcadoListVO = listAll(page);

        addActionMessageToVO(calcadoListVO, locale, messageKey, null);

        return returnListToCalcado(calcadoListVO);
    }

    private CalcadoListVO addActionMessageToVO(CalcadoListVO calcadoListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return calcadoListVO;
        }

        calcadoListVO.setActionMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return calcadoListVO;
    }

    private CalcadoListVO addSearchMessageToVO(CalcadoListVO calcadoListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return calcadoListVO;
        }

        calcadoListVO.setSearchMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return calcadoListVO;
    }

    private boolean isSearchActivated(String searchFor) {
        return !StringUtils.isEmpty(searchFor);
    }
}