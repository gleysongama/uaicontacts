package uaiContacts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import uaiContacts.model.Calcado;
import uaiContacts.repository.CalcadoRepository;
import uaiContacts.vo.CalcadoListVO;

@Service
@Transactional
public class CalcadoService {

	@Autowired
    private CalcadoRepository calcadoRepository;
	
	@Transactional(readOnly = true)
    public CalcadoListVO findAll(int page, int maxResults) {
        Page<Calcado> result = executeQueryFindAll(page, maxResults);

        if(shouldExecuteSameQueryInLastPage(page, result)){
            int lastPage = result.getTotalPages() - 1;
            result = executeQueryFindAll(lastPage, maxResults);
        }

        return buildResult(result);
    }

    public void save(Calcado calcado) {
        calcadoRepository.save(calcado);
    }

    @Secured("ROLE_ADMIN")
    public void delete(int calcadoId) {
        calcadoRepository.delete(calcadoId);
    }

    @Transactional(readOnly = true)
    public CalcadoListVO findByCorLike(int page, int maxResults, String cor) {
        Page<Calcado> result = executeQueryFindByCor(page, maxResults, cor);

        if(shouldExecuteSameQueryInLastPage(page, result)){
            int lastPage = result.getTotalPages() - 1;
            result = executeQueryFindByCor(lastPage, maxResults, cor);
        }

        return buildResult(result);
    }

    private boolean shouldExecuteSameQueryInLastPage(int page, Page<Calcado> result) {
        return isUserAfterOrOnLastPage(page, result) && hasDataInDataBase(result);
    }

    private Page<Calcado> executeQueryFindAll(int page, int maxResults) {
        final PageRequest pageRequest = new PageRequest(page, maxResults, sortByCorASC());

        return calcadoRepository.findAll(pageRequest);
    }

    private Sort sortByCorASC() {
        return new Sort(Sort.Direction.ASC, "cor");
    }

    private CalcadoListVO buildResult(Page<Calcado> result) {
        return new CalcadoListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());
    }

    private Page<Calcado> executeQueryFindByCor(int page, int maxResults, String cor) {
        final PageRequest pageRequest = new PageRequest(page, maxResults, sortByCorASC());

        return calcadoRepository.findByCorLike(pageRequest, "%" + cor + "%");
    }

    private boolean isUserAfterOrOnLastPage(int page, Page<Calcado> result) {
        return page >= result.getTotalPages() - 1;
    }

    private boolean hasDataInDataBase(Page<Calcado> result) {
        return result.getTotalElements() > 0;
    }
}
