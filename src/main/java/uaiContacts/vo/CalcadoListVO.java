package uaiContacts.vo;

import uaiContacts.model.Calcado;

import java.util.List;

public class CalcadoListVO {
    private int pagesCount;
    private long totalCalcados;

    private String actionMessage;
    private String searchMessage;

    private List<Calcado> calcados;

    public CalcadoListVO() {
    }

    public CalcadoListVO(int pages, long totalCalcados, List<Calcado> calcados) {
        this.pagesCount = pages;
        this.calcados = calcados;
        this.totalCalcados = totalCalcados;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public List<Calcado> getCalcados() {
        return calcados;
    }

    public void setCalcados(List<Calcado> calcados) {
        this.calcados = calcados;
    }

    public long getTotalCalcados() {
        return totalCalcados;
    }

    public void setTotalCalcados(long totalCalcados) {
        this.totalCalcados = totalCalcados;
    }

    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }
}